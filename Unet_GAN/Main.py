from Train import *
import os
from tensorflow import keras
from tensorflow.keras import layers
from pathlib import Path
from data_extractor import *
from tensorflow import keras
from tensorflow.keras import layers
from Model import *
from Predict import *

#VIDEOS
# baldguyFall.mp4
# ballToHead.mp4

LOAD_WEIGHTS = False
PREDICT = False
RUNNING_ON_COLAB = False

videoName = 'baldguyFall'

LEARNING_RATE = 0.0001
BATCH_SIZE = 4
NUM_EPOCHS = 2
# variables
prePath = ""
result_path = ""
if RUNNING_ON_COLAB:
    prePath = "/content/drive/My Drive/ni-mvi-svecpet8/"
    video_path =  prePath + "dataset_videos/"+ videoName +".mp4"
    result_path = prePath+"Unet_GAN/results/"
else:
    video_path =  str(Path(os.path.dirname(os.path.realpath(__file__))).parent) + '\\dataset_videos\\'+ videoName +'.mp4'
    result_path = os.path.dirname(os.path.realpath(__file__))+'\\results\\'
width= 368
height= 288
howManyTimesDoubleFPS = 1
FPScount = 20

#weights path
gan_weights_path = prePath + 'unet_gan_model_weights_'+videoName+'.h5'
generator_weights_path = prePath + 'unet_gan_generator_weights_'+videoName+'.h5'
discriminator_weights_path = prePath + 'unet_gan_discriminator_weights_'+videoName+'.h5'

# GAN configurations
input_shape_generator = (height,width,6)
shape_discriminator = (height,width,9)
d_model = define_discriminator(shape_discriminator, LEARNING_RATE,LOAD_WEIGHTS,discriminator_weights_path)
g_model = define_generator(input_shape_generator,LOAD_WEIGHTS,generator_weights_path)
gan_model = define_gan(g_model, d_model,LEARNING_RATE,LOAD_WEIGHTS,gan_weights_path)

if PREDICT == True:
    unet_gan_Predict(g_model,howManyTimesDoubleFPS, FPScount, video_path, videoName, width, height, BATCH_SIZE, result_path)
    unet_gan_CompareResults(g_model,video_path, videoName, width, height, BATCH_SIZE,result_path)
else:
    unet_gan_train(g_model,d_model,gan_model,video_path, videoName, width, height, LEARNING_RATE, NUM_EPOCHS, BATCH_SIZE,gan_weights_path,generator_weights_path,discriminator_weights_path)

