import os
from data_extractor import *
from tensorflow import keras
from tensorflow.keras import layers
from keras.callbacks import EarlyStopping

def unetTrain(model,video_path, videoName, width, height, LEARNING_RATE, NUM_EPOCHS, BATCH_SIZE,unet_weights_path):

      (x_train, y_train) = getDataToTrainModel(video_path,width,height)

      #save best weights
      earlyStop = EarlyStopping(
        monitor='loss',
        min_delta=0,
        patience=10,
        verbose=0,
        mode='auto',
        baseline=None,
        restore_best_weights=True
      )

      loss = "mse"
      optimizer = keras.optimizers.Adam(learning_rate=LEARNING_RATE)
      model.compile(loss=loss, optimizer=optimizer)
      #      callbacks=[earlyStop],

      model.fit(x_train, y_train,
            epochs=NUM_EPOCHS,
            batch_size=BATCH_SIZE,
            verbose=1
            )
      print("******save weights:*********")
      model.save_weights(unet_weights_path)
