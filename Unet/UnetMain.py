import os
from UnetModel import *
from tensorflow import keras
from tensorflow.keras import layers
from pathlib import Path
from UnetTrain import *
from UnetPredict import *
import tensorflow

#VIDEOS
# baldguyFall.mp4
# redBalls_trim.mp4

# 'baldguyFall'
# 'tarzan'
# 'pulp_fiction'
# 'red_balls_trim'
LOAD_WEIGHTS = True
PREDICT = False
RUNNING_ON_COLAB = False

videoName = 'tarzan'

LEARNING_RATE = 0.0001
BATCH_SIZE = 4
NUM_EPOCHS = 2


# variables
prePath = ""
if RUNNING_ON_COLAB:
    prePath = "/content/drive/My Drive/ni-mvi-svecpet8/"
    video_path =  prePath + "dataset_videos/"+ videoName +".mp4"
    result_path = prePath+"Unet/results/"
else:
    video_path =  str(Path(os.path.dirname(os.path.realpath(__file__))).parent) + '\\dataset_videos\\'+ videoName +'.mp4'
    result_path = os.path.dirname(os.path.realpath(__file__))+'\\results\\'

width= 368
height= 288
howManyTimesDoubleFPS = 1
FPScount = 20

#weights path
unet_weights_path = prePath + 'unet_weights_'+videoName+'.h5'

# unet configurations
input_shape = (height,width,6)
model = unet(input_shape)

if LOAD_WEIGHTS == True:
        model.load_weights(unet_weights_path)

if PREDICT == True:
    unetCompareResults(model,video_path, videoName, width, height, BATCH_SIZE, result_path)
    unetPredict(model,howManyTimesDoubleFPS ,FPScount,  video_path, videoName, width, height, BATCH_SIZE,result_path)
else:
    unetTrain(model,video_path, videoName, width, height, LEARNING_RATE, NUM_EPOCHS, BATCH_SIZE,unet_weights_path)

