import cv2
import numpy as np

def resizeImage(image, width, height):
    dim = (width,height)
    resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
    return resized

def videoToFrames(path,width, height):
    vidcap = cv2.VideoCapture(path)
    success,image = vidcap.read()

    frameCount = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))

    npVideo = np.empty((frameCount, height, width,3), np.float)

    position = 0
    while success:
        #image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = resizeImage(image, width,height)

        # Turn it into numpy, normalize and return.
        x = (image / 255.).astype(np.float32)
        npVideo[position] = x
        position = position+1
        success,image = vidcap.read()


    vidcap.release()
    return npVideo

def createDoubles(datasetArray):
    newArrLength = datasetArray.shape[0]-1
    concanatedArray = np.empty((newArrLength, datasetArray.shape[1], datasetArray.shape[2],6))
    for i in range(newArrLength):
        concanatedArray[i] = np.concatenate((datasetArray[i], datasetArray[i+1]), axis=2)
    return concanatedArray

# funkce (datasetArray)
# vystup [(X1,X3),X2]



def train_test_split(datasetArray):
    x_train_size = (len(datasetArray) // 2) +1
    y_train_size = x_train_size
    x_test_size = ((len(datasetArray) // 5) //2) +1
    y_test_size = x_test_size

    firstFrame = datasetArray[0]
    frameHeight = firstFrame.shape[0]
    frameWidth = firstFrame.shape[1]

    x_train = np.empty((x_train_size, frameHeight, frameWidth,3))
    y_train = np.empty((y_train_size, frameHeight, frameWidth,3))

    x_train_position = 0
    y_train_position = 0
    x_test_position = 0
    y_test_position = 0

    for i in range(len(datasetArray)) :
        value = datasetArray[i]

        if i % 2 == 0:
            x_train[x_train_position] = value
            x_train_position += 1
        else:
            y_train[y_train_position] = value
            y_train_position += 1

    x_train = createDoubles(x_train)
    trainLength = min(x_train.shape[0],y_train.shape[0])

    x_train = x_train[0:trainLength]
    y_train = y_train[0:trainLength]
    return (x_train, y_train)


def getDataToDoubleFPS(path,width,height):
    videoFrames = videoToFrames(path,width,height)
    return createDoubles(videoFrames)


def getDataToTrainModel(path,width,height):
    videoFrames = videoToFrames(path,width,height)
    return train_test_split(videoFrames)

def getDataToTrainGANModel(path,width,height):
    (x1x3_train, x2_train) = getDataToTrainModel(path,width,height)
    resizedFrames  = np.array([resizeImage(frame, width-10,height-10) for frame in x2_train])
    return (x1x3_train, resizedFrames)


def mergeArrays(frames1, frames2):
    minLength = min(frames1.shape[0], frames2.shape[0])
    frames1 = frames1[0:minLength]
    frames2 = frames2[0:minLength]

    mergedFrames = np.empty(
        (minLength*2, frames1.shape[1], frames1.shape[2], 3))
    mergedFrames[0::2] = frames1
    mergedFrames[1::2] = frames2
    return mergedFrames
