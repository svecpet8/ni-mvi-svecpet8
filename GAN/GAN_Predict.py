from tensorflow import keras
from tensorflow.keras import layers
from data_extractor import *
import cv2
import os

import matplotlib.animation as animation
import matplotlib.pyplot as plt

def ganCompareResults(model,video_path, videoName, width, height, BATCH_SIZE,resultPath):

    (x_train, y_train) = getDataToTrainModel(video_path,width,height)

    y_predicted = model.predict(x_train, batch_size=BATCH_SIZE)

    # uložit nějaký frame pro porovnání..
    photopath = resultPath+videoName+'_image.png'
    cv2.imwrite(photopath,y_predicted[10]*255)
    photopath = resultPath+videoName+'_image_orig.png'
    cv2.imwrite(photopath,y_train[10]*255)


def gan_Predict(model, howManyTimesDoubleFPS, originalVideoFPS, video_path, videoName, width, height, BATCH_SIZE,resultPath):

    allFrames = videoToFrames(video_path, width, height)


    newFPS = originalVideoFPS * (2 * howManyTimesDoubleFPS)

    originalVideo = allFrames.copy()


    for x in range(0, howManyTimesDoubleFPS):
        predictFrames = createDoubles(allFrames)

        newFrames = model.predict(predictFrames, batch_size=BATCH_SIZE)
        # resize Frames
        newFrames  = np.array([resizeImage(frame, width,height) for frame in newFrames])

        allFrames = mergeArrays(allFrames, newFrames)


    # recover normalization
    for i in range(len(allFrames)):
        allFrames[i] = allFrames[i]*255
    allFrames = allFrames.astype(np.uint8)

    for i in range(len(originalVideo)):
        originalVideo[i] = originalVideo[i]*255
    originalVideo = originalVideo.astype(np.uint8)

    for i in range(len(newFrames)):
        newFrames[i] = newFrames[i]*255
    newFrames = newFrames.astype(np.uint8)

    # save original video (different width and height)
    outOrig = cv2.VideoWriter( resultPath+videoName+'_video_orig.mp4',cv2.VideoWriter_fourcc(*'mp4v'), originalVideoFPS, (width,height),True)
    for i in range(len(originalVideo)):
        outOrig.write(originalVideo[i])
    outOrig.release()

    # save all frames video
    outAll = cv2.VideoWriter( resultPath+videoName+'_video.mp4',cv2.VideoWriter_fourcc(*'mp4v'), newFPS, (width,height),True)
    for i in range(len(allFrames)):
        outAll.write(allFrames[i])
    outAll.release()

    # save video only from last computed frames
    outComputed = cv2.VideoWriter( resultPath + videoName+'_onlyComputedFrames_video.mp4',cv2.VideoWriter_fourcc(*'mp4v'), originalVideoFPS, (width,height),True)
    for i in range(len(newFrames)):
        outComputed.write(newFrames[i])
    outComputed.release()

