import tensorflow as tf
from numpy.random import rand
from numpy.random import randn

import numpy as np
from keras.optimizers import Adam
from keras.models import *
from keras.layers import *
from matplotlib import pyplot as plt
from keras.layers.experimental.preprocessing import *


def define_generator(input_shape, LOAD_WEIGHTS,weights_path):
    # input shape - neubira se -3 vždy?

    in_label = Input(
        shape=(input_shape[0], input_shape[1], input_shape[2]), name='label')

    # zkusit Wasserstein Loss
    conv1 = Convolution2D(48, (3, 3), activation='relu')(in_label)
    bn1 = BatchNormalization()(conv1)

    conv2 = Convolution2D(24, (3, 3), activation='relu')(bn1)
    bn2 = BatchNormalization()(conv2)

    conv3 = Convolution2D(12, (3, 3), activation='relu')(bn2)
    bn3 = BatchNormalization()(conv3)

    conv4 = Convolution2D(12, (3, 3), activation='relu')(bn3)
    bn4 = BatchNormalization()(conv4)

    out_layer = Convolution2D(3, (3, 3), activation='relu')(bn4)
    model = Model(in_label, out_layer)
    if LOAD_WEIGHTS:
        model.load_weights(weights_path)
    return model


def define_discriminator(input_shape, learning_rate, LOAD_WEIGHTS, weights_path):
    in_label = Input(
        shape=(input_shape[0], input_shape[1], input_shape[2]), name='input_label')

    # jak dostat pulku vstupniho pool1 = MaxPooling2D(pool_size=(2, 2))(bn1)  X input_shape[2]//2
    conv1 = Convolution2D(24, (3, 3), activation='relu',
                          padding='same', name="conv1")(in_label)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Convolution2D(96, (3, 3), activation='relu',
                          padding='same', name="conv2")(pool1)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Convolution2D(96, (3, 3), activation='relu',
                          padding='same', name="conv3")(pool2)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

    flatted = Flatten()(pool3)
    #
    neurons = Dense(1024, name="neurons")(flatted)

    out_layer = Dense(1, activation='sigmoid', name="out_layer")(neurons)

    model = Model(in_label, out_layer)

    opt = Adam(lr=learning_rate)
    if LOAD_WEIGHTS:
        model.load_weights(weights_path)
    model.compile(loss='binary_crossentropy',
                  optimizer=opt, metrics=['accuracy'])

    return model


def define_gan(g_model, d_model, learning_rate,LOAD_WEIGHTS,weights_path):
    d_model.trainable = False
    gen_output = g_model.output
    #merged_layers = Concatenate()([g_model.input,gen_output])
    gan_output = d_model(gen_output)
    model = Model(g_model.input, gan_output)
    opt = Adam(lr=learning_rate)
    if LOAD_WEIGHTS == True:
        model.load_weights(weights_path)
    model.compile(loss='binary_crossentropy', optimizer=opt)
    return model
