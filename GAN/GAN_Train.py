import os
from data_extractor import *
from tensorflow import keras
from tensorflow.keras import layers



def gan_train(g_model, d_model, gan_model, video_path, videoName, width, height, LEARNING_RATE, NUM_EPOCHS, BATCH_SIZE,gan_weights_path,generator_weights_path,discriminator_weights_path):
    (x1x3_train, x2_train) = getDataToTrainGANModel(video_path, width, height)

    bat_per_epo = int(x1x3_train.shape[0] / BATCH_SIZE)
    half_batch = int(BATCH_SIZE / 2)
    ganLoss = 0
    discrimLoss = 0
    for i in range(NUM_EPOCHS):
        print("epoch ----"+ str(i))
        print("gan Loss"+ str(ganLoss))
        print("discriminator Loss"+ str(discrimLoss))

        for j in range(bat_per_epo):
            # snímky
            startIndex = j*(BATCH_SIZE)
            endIndex = min((j+1)*(BATCH_SIZE),x1x3_train.shape[0])
            x1x3_batch = x1x3_train[startIndex:endIndex]
            x2_batch = x2_train[startIndex:endIndex]
            x2_batch_fake  = g_model.predict(x1x3_batch)

            concatBatch = np.concatenate((x2_batch, x2_batch_fake))
            concatBatch_res = np.concatenate(( (np.ones((x2_batch.shape[0], 1))), np.zeros((x2_batch.shape[0], 1))) )

            d_loss1, _ = d_model.train_on_batch(concatBatch,concatBatch_res)


            g_loss = gan_model.train_on_batch(
                x1x3_batch, np.ones((x2_batch.shape[0], 1)))
            ganLoss = g_loss
            discrimLoss = d_loss1


    print("******save weights:*********")
    g_model.save_weights(generator_weights_path)
    d_model.save_weights(discriminator_weights_path)
    gan_model.save_weights(gan_weights_path)
