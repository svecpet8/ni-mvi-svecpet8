Název práce: 6) Video Frame Rate Upscaling Using Neural Networks

Popis práce: cílem je možnost pouštět si pomaleji GIF videa motivační příklad:  ( https://media.tenor.com/videos/2809cfd0c1e69be6797de9d59b45fada/mp4 )

Videa zvolená k trénování dostupná zde:  https://drive.google.com/drive/folders/1PlkbT7tHYIbIVUvk-tcMWQrsOs_S0FX9?usp=sharing

Pokyny ke spuštění: pro daný model se pouští daný soubor main (zadejte v souboru název videa, který máte uložen ve složce dataset_videos)

výsledky s prezentací na odkaze: https://rb.gy/4ppnkd
